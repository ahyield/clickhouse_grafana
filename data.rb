require 'bundler/inline'

gemfile do
  source 'https://rubygems.org'
  gem 'click_house'
  gem 'oj'
end

ClickHouse.config do |config|
  #config.logger = Logger.new(STDOUT)
  config.adapter = :net_http
  config.database = 'default'
  config.url = 'http://localhost:8123'
  config.timeout = 60
  config.open_timeout = 3
  config.ssl_verify = false
  # set to true to symbolize keys for SELECT and INSERT statements (type casting)
  config.symbolize_keys = false
  config.headers = {}

  # if you use HTTP basic Auth
  config.username = 'default'
  config.password = 'default'

  # or Oj parser
  config.json_parser = ClickHouse::Middleware::ParseJsonOj

  # or Oj.dump
  config.json_serializer = ClickHouse::Serializer::JsonOjSerializer
end

ClickHouse.connection.execute <<~SQL
CREATE TABLE metrics
(
    id UInt32,
    timestamp DateTime,
    metric Float32,
    version String,
    target_env String,
    status String
)
ENGINE = MergeTree
PRIMARY KEY (id, timestamp, target_env)
SQL


require 'securerandom'

query = <<-SQL
  INSERT INTO metrics (id, timestamp, metric, version, target_env, status) VALUES
SQL

1000.times do |i|
  random_timestamp = (Time.now - rand(1..30) * 86400).strftime('%Y-%m-%d %H:%M:%S')
  random_version = "15.11.20230328#{format('%04d', rand(1..9999))}-#{SecureRandom.hex(8)}"

  query << if i < 999
    <<-ROW
      (#{10001 + i}, '#{random_timestamp}', 1.0, '#{random_version}', 'gprd', 'completed'),
    ROW
  else
    <<-ROW
      (#{10001 + i}, '#{random_timestamp}', 1.0, '#{random_version}', 'gprd', 'completed');
    ROW
  end
end


ClickHouse.connection.execute query

query = <<-SQL
  INSERT INTO metrics (id, timestamp, metric, version, target_env, status) VALUES
SQL

1000.times do |i|
  random_timestamp = (Time.now - rand(1..30) * 86400).strftime('%Y-%m-%d %H:%M:%S')
  random_version = "15.11.20230328#{format('%04d', rand(1..9999))}-#{SecureRandom.hex(8)}"

  query << if i < 999
    <<-ROW
      (#{10001 + i}, '#{random_timestamp}', 1.0, '#{random_version}', 'gprd', 'started'),
    ROW
  else
    <<-ROW
      (#{10001 + i}, '#{random_timestamp}', 1.0, '#{random_version}', 'gprd', 'started');
    ROW
  end
end


ClickHouse.connection.execute query

